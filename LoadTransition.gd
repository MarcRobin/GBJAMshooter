extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var nextscene : PackedScene
var has_started

# Called when the node enters the scene tree for the first time.
func _ready():
	has_started = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !has_started:
		trigger_loading_animation()
		has_started = true

func trigger_loading_animation(anim = "start"):#start or end animation
	$AnimatedSprite.visible = true
	$AnimatedSprite.play(anim)
	$AnimatedSprite.frame = 0 #depending on start or end change animation
	


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "end":
		get_tree().change_scene_to(nextscene)
	elif $AnimatedSprite.animation == "start":
		$AnimatedSprite.visible = false
