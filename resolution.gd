extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#yield(get_tree(), "idle_frame")
	var scrsize = OS.get_screen_size()
	var m = floor(min(scrsize.x/160, scrsize.y/144))
	if 160 * m == scrsize.x or 144 * m == scrsize.y:
		m -= 1
	OS.set_window_size(Vector2(160*m,144*m))
	OS.center_window()
	#OS.get_screen_scale()
	print(OS.get_name())
	print(OS.get_window_safe_area())
	print(m, " --- ", OS.get_window_size())
	if "Windows" in OS.get_name() and false:
		if OS.window_position.y + OS.window_size.y > OS.get_window_safe_area().end.y:
			if OS.window_size.y <= OS.get_window_safe_area().end.y:
				OS.window_position.y = floor((OS.get_window_safe_area().end.y - OS.window_size.y)/2)
			else:
				OS.set_window_size(Vector2(160*(m-1),144*(m-1)))
				OS.center_window()
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if "Windows" in OS.get_name():
		if Input.is_key_pressed(KEY_6) and Input.is_key_pressed(KEY_CONTROL):
			OS.set_window_size(Vector2((OS.get_window_size().x/160-1)*160,(OS.get_window_size().x/160-1)*144))
			OS.center_window()
			print(OS.get_window_size().x/160, " --- ", OS.get_window_size())
			#OS.set_window_size(OS.get_screen_size())
			#OS.center_window()
			#OS.request_attention()
			#print(OS.get_window_safe_area())
			#print( get_viewport().get_visible_rect())
