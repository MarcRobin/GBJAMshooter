tool

extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal store_position1
signal reset_position1
var group_str = "Icon"

# Called when the node enters the scene tree for the first time.
func _ready():
	#position = Vector2(80, O)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x = round(position.x)
	position.y = round(position.y)
	if Engine.editor_hint:
		if Input.is_action_just_pressed("ui_cancel"):
			print("nbchild: ", get_child_count(), " ", name)
			if get_child_count() > 0:
				yield(get_child(0), "store_position1")
			var pos = global_position
			print("store_position ", name)
			emit_signal("store_position1")
			print("after_store_position ", name)
			#if "icon" in get_parent().name:
			#	yield(get_parent(), "reset_position1")
			global_rotation_degrees = 0
			pos.x = round(pos.x)
			pos.y = round(pos.y)
			global_position = pos
			print("reset_position ", name)
			emit_signal("reset_position1")
			print("after_reset_position ", name)
		if Input.is_key_pressed(KEY_KP_SUBTRACT) and Input.is_action_just_pressed("ui_accept"):
			if group_str in get_parent().name:
				position -= position.normalized()
		elif Input.is_action_just_pressed("ui_accept") and Input.is_key_pressed(KEY_KP_ADD):#Input.is_action_just_pressed("ui_focus_next"):
			if group_str in get_parent().name:
				position += position.normalized()
		if Input.is_action_pressed("ui_accept") and Input.is_key_pressed(KEY_V):
			if group_str in get_parent().name:
				position += position.normalized()
		if Input.is_action_pressed("ui_accept") and Input.is_key_pressed(KEY_B):
			if group_str in get_parent().name:
				position -= position.normalized()
		#if Input.is_key_pressed(KEY_CONTROL) and Input.is_action_just_pressed("ui_up"):
		
		if Input.is_action_just_pressed("ui_focus_next"):
			if group_str == "Icon":
				group_str = "Icom"
			else:
				group_str = "Icon"
